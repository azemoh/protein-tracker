import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Users, History } from '../api/collections';

import './main.html';

Template.userDetails.helpers({
  user() {
    return Users.findOne()
  }
});

Template.userDetails.events({
  'click #addAmount'(e) {
    e.preventDefault();
    const amount = parseInt($('#amount').val());
    if (amount) {
      Users.update(this._id, { $inc: { total: amount } });
      History.insert({
        amount: amount,
        date: new Date().toTimeString(),
        userId: this._id
      })
    }

  }
})

Template.history.helpers({
  entry() {
    return History.find({}, { sort: { date: -1 }, limit: 5});
  }
});
