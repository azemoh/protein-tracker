import { Meteor } from 'meteor/meteor';
import { Users, History } from '../api/collections';

Meteor.startup(() => {
  // code to run on server at startup

  if (Users.find().count() === 0) {
    Users.insert({ total: 120, goal: 200 });
  }

});
