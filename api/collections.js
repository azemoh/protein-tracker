import { Mongo } from 'meteor/mongo'

export const Users = new Mongo.Collection('users');
export const History = new Mongo.Collection('history');
